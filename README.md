# Fritzbox Callcenter Telefonbuch Generator

Dieses Skript generiert Telefonbücher für die Fritzbox mit sämtlichen Callcenter Nummern des Ktipps.
Es können auch einfach die Telefonbücher aus dem phonebooks Ordner heruntergeladen werden und in die Fritzbox importiert werden.

Zusätzlich zu den Fritbox Telefonbüchern wird auch eine NumberList.txt generiert in welcher nur die Telefonnummern gespeichert werden. Diese können dann z.B. für [NCID](http://ncid.sourceforge.net/) verwendet werden.

Nummern mit Kommentaren oder Text darin werden aussortiert.

## Voraussetzungen
- Internet Zugang

## Ausführung
Einfach das Skript auf der Powershell Konsole starten.

```
ktippparser.ps1
```

## Direkter Download
Die Listen werden regelmässig mit einer GitLab Pipeline generiert und können hier heruntergeladen werden:

### NCID Nummerliste
- [NumberList.txt](https://mlueckert.gitlab.io/ktipp_phonebooks/NumberList.txt)
### Fritzbox Telefonbücher
- [CallCenterPhonebook-1.xml](https://mlueckert.gitlab.io/ktipp_phonebooks/CallCenterPhonebook-1.xml)
- [CallCenterPhonebook-2.xml](https://mlueckert.gitlab.io/ktipp_phonebooks/CallCenterPhonebook-2.xml)
- [CallCenterPhonebook-3.xml](https://mlueckert.gitlab.io/ktipp_phonebooks/CallCenterPhonebook-3.xml)
- [CallCenterPhonebook-4.xml](https://mlueckert.gitlab.io/ktipp_phonebooks/CallCenterPhonebook-4.xml)